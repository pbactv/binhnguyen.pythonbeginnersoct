a = 10
s = 'hello'
w = 'world'
print(s + ' ' + w , a ,'times')
print('{} {} {} times'.format(s,w,a))
x = 12.2413579
import math
pi = math.pi
print('x = {:.3f}'.format(x*pi))

s = 'hello python'
print(str(s))
print(repr(s))
print(str(1/7))
print('int value ' + repr(4.0))
print('int value ' + str(4.0))
print((repr('hello\n')))
k = repr((30, 50.2, ('one','two')))
print(k)
