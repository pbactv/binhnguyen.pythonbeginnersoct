#Create a tuple t of 4 elements, that are: 1, 'python', [2, 3], (4, 5)
t = 1, 'python', [2,3], (4, 5)
print(t)
var1, var2, [var3, var4], (var5, var6) = t
print(var1, var2, var3, var4, var5, var6)
#Print out the last element of t
print('The last element of t', t[-1])

#Add to t a list [2,3]
alist = ([2,3],)
t = t + alist
print(t)

#Check whether list [2, 3] is duplicated in t
x = t.count([2,3])
print('The number of [2,3] in t = ', x)

#Remove list [2, 3] from t
t=tuple(item for item in t if t.count(item) == 1)
print(t)

#Convert tuple t into a list
tlist = list(t)
print(tlist)
