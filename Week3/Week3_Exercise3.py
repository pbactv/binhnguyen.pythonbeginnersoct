#Write a Python script to concatenate following dictionaries to create a new one.
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50, 6:60}
newdic = {**dic1, **dic2, **dic3}
print(newdic)
print(list(dict(newdic).keys()))
print(sorted(newdic, key=newdic.get))

#Write a Python script to print a dictionary where the keys are numbers between 1 and 15 (both included) and the values are square of keys.
d = dict()
for x in range(1,16):
    d[x]=x**2
print(d)

#Write a Python script to sort ascending a dictionary by value.
from operator import itemgetter
dic4 = {'a': 1, 'b': 7, 'e': 9, 'c': 2, 'd': 5}
print(sorted(dic4, key=dic4.get))
sorted_x = sorted(dic4.items(), key=itemgetter(1))
print(list(dict(sorted_x).keys()))


#Write a script to create a dictionary where the keys are unique characters of the string and the mapped values are their occurence in the string.
import collections
string = ('Python is an easy language to learn')
print(dict(collections.Counter(string.replace(' ',''))))

t = "Python is an easy language to learn"
t = t.replace(' ','')
print(t)
print(dict((c,t.count(c)) for c in set(t)))



