setA = {1, 2, 3, 4, 5, 6, 7}
setB = {2, 4, 5, 9, 12, 24}
setC = {2, 4, 8}
#Iterate over all elements of set C
for i in setC:
    print(i)
setA.update(setC)
setB.update(setC)
#Print out A and B after adding elements
print(setA, setB)
#Print out the intersection of A and B
print(setA.intersection(setB))
#Print out the union of A and B
print(setA.union(setB))
#Print out elements in A but not in B
print(setA.difference(setB))
#Print out the length of A and B
print(len(setA),len(setB))
#Print out the maximum value of A union B
print(max(setA.union(setB)))
#Print out the minimum value of A union B
print(min(setA.union(setB)))